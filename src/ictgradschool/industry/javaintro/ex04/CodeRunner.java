package ictgradschool.industry.javaintro.ex04;

import java.util.Arrays;

/**
 * Programming for Industry - Lab 01
 * Exercise Four - Sorting Numbers
 * 
 * Note: Put your answers in between the "// Answer here //" comments. Do not modify other parts of the class.
 */
public class CodeRunner {
	
	/**
	 * Sorting numbers in order.
	 */
	public void sortNumberByAscending(int number1, int number2, int number3, int number4) {
		int first = 0;
		int second = 0;
		int third = 0;
		int fourth = 0;
		// Answer here

		//int[] myIntArray = new int[]{number1,number2,number3,number4};

		first = Math.min (number1,Math.min(number2,Math.min(number3,number4)));
		fourth = Math.max (number1,Math.max(number2,Math.max(number3,number4)));
		// second = Math.max (number1,Math.min(number2,Math.min(number3,number4)));
		// third = Math.max (number1,Math.max(number2,Math.min(number3,number4)));



		int a1 = Math.min (number1,number2);
		int a2 = Math.min (number1,number3);
		int a3 = Math.min (number1,number4);
		int b1 = Math.min (number2,number3);
		int b2 = Math.min (number2,number4);
		int c1 = Math.min (number3,number4);
		int d1 = Math.min (a1, a2);
		int d2 = Math.min (a2, a3);


		//second = Math.max (first, Math.min(d1, d2));
		//third = Math.min (fourth, Math.min(d1,d2));
		second = Math.min (second,Math.min(fourth, Math.min (first, Math.max (number1,Math.min(number2,Math.min(number3,number4))))));
		third = Math.max (first, Math.max (fourth, Math.min (number1,Math.max(number2,Math.max(number3,number4)))));

		//Arrays.sort; int from; Math.max((number1,Math.min(number2,Math.min(number3,number4))) int to Math.max(number1,Math.max(number2,Math.max(number3,number4)));

		//first = Math.min (number1,Math.min(number2,Math.min(number3,number4)));
		//fourth = Math.max (number1,Math.max(number2,Math.max(number3,number4)));

		


		//
		System.out.println("The numbers are: " + first + ", " + second + ", " + third + ", " + fourth);
	}

	/**
	 * Don't edit this - but read/use this for testing if you like.
	 */
	public static void main(String[] args) {
		CodeRunner cr = new CodeRunner();
		
		cr.sortNumberByAscending(35, -4, 7, 6); // The numbers are: -4, 6, 7, 35
		cr.sortNumberByAscending(-1, 0, 18, -10); // The numbers are: -10, -1, 0, 18
		cr.sortNumberByAscending(1, 2, 3, 4); // The numbers are: 1, 2, 3, 4
	}

}
